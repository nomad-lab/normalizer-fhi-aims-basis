/*
 * Copyright 2017-2018 Fawzi Mohamed, Danio Brambila
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package eu.nomad_lab.normalizers

import eu.{ nomad_lab => lab }
import eu.nomad_lab.DefaultPythonInterpreter
import org.{ json4s => jn }
import scala.collection.breakOut
import eu.nomad_lab.normalize.ExternalNormalizerGenerator
import eu.nomad_lab.normalize.Normalizer
import eu.nomad_lab.meta
import eu.nomad_lab.query
import eu.nomad_lab.resolve._
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.h5.H5EagerScanner
import eu.nomad_lab.parsers.ExternalParserWrapper
import eu.nomad_lab.JsonUtils
import scala.collection.mutable.StringBuilder

object FhiAimsBasisNormalizer extends ExternalNormalizerGenerator(
  name = "FhiAimsBasisNormalizer",
  info = jn.JObject(
    ("name" -> jn.JString("FhiAimsBasisNormalizer")) ::
      ("parserId" -> jn.JString("FhiAimsBasisNormalizer" + lab.FhiAimsBasisVersionInfo.version)) ::
      ("versionInfo" -> jn.JObject(
        ("nomadCoreVersion" -> jn.JObject(lab.NomadCoreVersionInfo.toMap.map {
          case (k, v) => k -> jn.JString(v.toString)
        }(breakOut): List[(String, jn.JString)])) ::
          (lab.FhiAimsBasisVersionInfo.toMap.map {
            case (key, value) =>
              (key -> jn.JString(value.toString))
          }(breakOut): List[(String, jn.JString)])
      )) :: Nil
  ),
  context = "calculation_context",
  filter = query.CompiledQuery(query.QueryExpression("program_name = \"FHI-aims\" and x_fhi_aims_section_controlin_basis_set"), meta.KnownMetaInfoEnvs.fhiAims),
  cmd = Seq(DefaultPythonInterpreter.pythonExe(), "${envDir}/normalizers/fhi-aims-basis/normalizer/normalizer-fhi-aims-basis/base_comparison.py",
    "${contextUri}", "${archivePath}"),
  resList = Seq(
    "normalizer-fhi-aims-basis/base_comparison.py",
    "normalizer-fhi-aims-basis/setup_paths.py",
    "normalizer-fhi-aims-basis/json_default/light.json",
    "normalizer-fhi-aims-basis/json_default/really_tight.json",
    "normalizer-fhi-aims-basis/json_default/tight.json",
    "nomad_meta_info/public.nomadmetainfo.json",
    "nomad_meta_info/common.nomadmetainfo.json",
    "nomad_meta_info/meta_types.nomadmetainfo.json",
    "nomad_meta_info/stats.nomadmetainfo.json"
  ) ++ DefaultPythonInterpreter.commonFiles(),
  dirMap = Map(
    "normalizer-fhi-aims-basis" -> "normalizers/fhi-aims-basis/normalizer/normalizer-fhi-aims-basis",
    "nomad_meta_info" -> "nomad-meta-info/meta_info/nomad_meta_info",
    "python" -> "python-common/common/python/nomadcore"
  ) ++ DefaultPythonInterpreter.commonDirMapping(),
  metaInfoEnv = lab.meta.KnownMetaInfoEnvs.stats

) {

  override def stdInHandler(context: ResolvedRef)(wrapper: ExternalParserWrapper)(pIn: java.io.OutputStream): Unit = {
    val out: java.io.Writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(pIn));
    val trace: Boolean = Normalizer.trace
    val stringBuilder = if (trace)
      new StringBuilder
    else
      null
    def writeOut(s: String): Unit = {
      out.write(s)
      if (trace) stringBuilder ++= s
    }
    def flush(): Unit = {
      out.flush()
      if (trace) {
        logger.info(stringBuilder.result())
        stringBuilder.clear()
      }
    }
    writeOut("[")
    var isFirst = true
    try {
      context match {
        case Calculation(archiveSet, c) =>
          for (m <- c.sectionTable(Seq("section_run", "section_method"))) {
            if (!isFirst)
              writeOut(",")
            else
              isFirst = false
            writeOut(s"""{
                          |  "context": ${JsonUtils.escapeString(m.toRef.toUriStr(archiveSet.objectKind))},
                          |  "section_method": """.stripMargin)
            val visitor = new EmitJsonVisitor(
              writeOut = writeOut
            )
            val scanner = new H5EagerScanner
            scanner.scanResolvedRef(Section(archiveSet, m), visitor)
            writeOut("}")
            flush()
          }
          writeOut("]")
          flush()
        case r =>
          throw new Exception(s"FhiAimsBasisNormalizer expected a calculation as context, but got $r")
      }
    } finally {
      out.close()
      pIn.close()
      wrapper.sendStatus = ExternalParserWrapper.SendStatus.Finished
    }
  }

}
