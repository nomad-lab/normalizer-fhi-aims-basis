# Copyright 2017-2018 Fawzi Mohamed, Danio Brambila
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import json
from  numpy import *


# Open json files
def open_json(path):
  default=dict()
  with open(path) as data_file:    
    default = json.load(data_file)
  return default

# Finds out if val is in the array 
def CompareList(val,list):
  if val in list:
    return 0
  return 1

# Comparison between two dicts. 
# List structure:
def CompareTwoDicts(d1,d2):
  sum2=zeros(len(d2))

# Loop over the size of dict2
  for k in arange(0,len(d2)):
#   Lopp over the elements of each dict2 
    for idx,val in d1.items():
      if (idx not in ["gIndex","references"]) : # Excludes the keys that are always different.
        try:
          if (val!=d2[k][idx]):
            sum2[k]=sum2[k]+1
        except KeyError: # this exception case arises if the cut off potential is not a number
          continue 
  if (min(sum2)==0):
    return 0
  else:
    return sum(sum2)

def CompareToDefaults(dict2_default,dict1):

#first compare the integration grid
  false_hits=0
  for key in dict1:
    if (key!="gIndex") and (key != "x_fhi_aims_section_controlin_basis_func") and size(dict1[key])==1:
      if(dict1[key]!=dict2_default[key]):
        false_hits+=1
      false_hits+=abs(size(dict1[key])-size(dict2_default[key]))
    if size(dict1[key])>1:
      if (key != "x_fhi_aims_section_controlin_basis_func"):
        for i in dict1[key]:
         false_hits+=CompareList(i,dict2_default[key])
#        false_hits+=abs(size(dict1[key])-size(dict2_default[key]))

      if (key == "x_fhi_aims_section_controlin_basis_func"):
        for i in arange(0,len(dict1[key])):
            false_hits+=CompareTwoDicts(dict1[key][i],dict2_default[key])
      false_hits+=abs(len(dict1[key])-len(dict2_default[key]))
  return false_hits

path='/home/brambila/json_default/example.json'
import sys, getopt

def main(argv):

  inputfile = ''
  outputfile = ''
  try:
     opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
  except getopt.GetoptError:
     print 'test.py -i <inputfile> -o <outputfile>'
     sys.exit(2)
  for opt, arg in opts:
     if opt == '-h':
       print 'test.py -i <inputfile> -o <outputfile>'
       sys.exit()
     elif opt in ("-i", "--ifile"):
       inputfile = arg
     elif opt in ("-o", "--ofile"):
       outputfile = arg
#default /home/brambila/json_default/example.json
  ParsedFile='/home/brambila/json_default/example.json'
  folder_default='/home/brambila/json_default/default_'
  format='.json'
  pure_types=['light','tight','really_tight']
#                0       1          2
  defaults=[]

  with open("/home/brambila/json_default/default_light.json") as data_file:    
    light = json.load(data_file)
  with open("/home/brambila/json_default/default_tight.json") as data_file:    
    tight = json.load(data_file)
  with open("/home/brambila/json_default/default_really_tight.json") as data_file:    
    really_tight = json.load(data_file)
  with open(inputfile) as data_file:    
    to_compare = json.load(data_file)


  light=light["sections"]["section_run-0"]["sections"]["section_method-0"]["x_fhi_aims_section_controlin_basis_set"]
  tight=tight["sections"]["section_run-0"]["sections"]["section_method-0"]["x_fhi_aims_section_controlin_basis_set"]
  really_tight=really_tight["sections"]["section_run-0"]["sections"]["section_method-0"]["x_fhi_aims_section_controlin_basis_set"]
  to_compare=to_compare["sections"]["section_run-0"]["sections"]["section_method-0"]["x_fhi_aims_section_controlin_basis_set"]



  Natoms=len(to_compare)
  matrix_hits=zeros(3*Natoms).reshape(3,Natoms)

  for i in arange(0,Natoms):
    AtomIndex=int(to_compare[i]["x_fhi_aims_controlin_nucleus"])
    matrix_hits[0,i]  =  CompareToDefaults(light[AtomIndex],to_compare[i])
    matrix_hits[1,i]  =  CompareToDefaults(tight[AtomIndex],to_compare[i])
    matrix_hits[2,i]  =  CompareToDefaults(really_tight[AtomIndex],to_compare[i])

  print matrix_hits
  summary_hits=sum(matrix_hits,axis=1)

  for idx,item in enumerate(summary_hits):
    if (item==0):
      print pure_types[idx]
      return pure_types[idx]

  if min(summary_hits)<5 :
    print pure_types[argmin(summary_hits)]+'+'
    return pure_types[argmin(summary_hits)]+'+'

  print "custom"
  return "custom"

if __name__ == "__main__":
    main(sys.argv[1:])

