################################################################################
#
#  FHI-aims code project
# Volker Blum, Fritz Haber Institute Berlin, 2009
#
#  Suggested "tight" defaults for C atom (to be pasted into control.in file)
#
################################################################################
  species        C
#     global species definitions
    nucleus             6
    mass                12.0107
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         34 7.0
    radial_multiplier   2
    angular_grids specified
      division   0.2187   50
      division   0.4416  110
      division   0.6335  194
      division   0.7727  302
      division   0.8772  434
#      division   0.9334  590
#      division   0.9924  770
#      division   1.0230  974
#      division   1.5020 1202
#     outer_grid  974
      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   2.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   1.
################################################################################
#     
#  Suggested additional basis functions. For production calculations,
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.0 A, 1.25 A, 1.5 A, 2.0 A, 3.0 A
#
################################################################################
#  "First tier" - improvements: -1214.57 meV to -155.61 meV
     hydro 2 p 1.7
     hydro 3 d 6
     hydro 2 s 4.9
#  "Second tier" - improvements: -67.75 meV to -5.23 meV
#     hydro 4 f 9.8
     hydro 3 p 5.2
#     hydro 3 s 4.3
     hydro 5 g 14.4
#     hydro 3 d 6.2
################################################################################
#
#  FHI-aims code project
# Volker Blum, Fritz Haber Institute Berlin, 2009
#
#  Suggested "tight" defaults for Mg atom (to be pasted into control.in file)
#
#  2016/03/22 : Included the tier2 f function by default. This is
#               certainly necessary to warrant the designation
#               "tight". Note that convergence tests by including
#               further individual radial functions from tier2 may be
#               a good idea anyway.
#
#  2016/03/28 : After further discussions, added full tier 2 to default basis
#               set (this is "tight" after all). Note that, particularly for
#               hybrid functionals, it may be possible to get excellent accuracy
#               using just tier 1 + the f function from tier 2, at much reduced
#               cost.
#
################################################################################
  species        Mg
#     global species definitions
    nucleus             12
    mass                24.3050
#
    l_hartree           6
#
    cut_pot             5.0          2.0  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         40 7.0
    radial_multiplier   2
    angular_grids       specified
      division   0.5421   50
      division   0.8500  110
      division   1.0736  194
      division   1.1879  302
      division   1.2806  434
#      division   1.4147  590
#      division   1.4867  770
#      division   1.6422  974
#      division   2.6134 1202
#      outer_grid   974
      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      3  s   2.
    valence      2  p   6.
#     ion occupancy
    ion_occ      2  s   2.
    ion_occ      2  p   6.
################################################################################
################################################################################
#
#  Suggested additional basis functions. For production calculations,
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 2.125 A, 2.375 A, 2.875 A, 3.375 A, 4.5 A
#
################################################################################
#  "First tier" - improvements: -230.76 meV to -21.94 meV
     hydro 2 p 1.5
     ionic 3 d auto
     hydro 3 s 2.4
#  "Second tier" - improvements: -5.43 meV to -1.64 meV
     hydro 4 f 4.3
     hydro 2 p 3.4
     hydro 4 s 11.2
     hydro 3 d 6.2

